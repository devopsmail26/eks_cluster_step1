
provider "aws" {

    region = "us-east-1"
  
}

terraform {
  backend "s3" {
    bucket = "aj27-terraform-s3-backend"
    key    = "tfstate_file/harness_test/"
    region = "us-east-1"
  }
}



resource "aws_vpc" "main" {
  cidr_block       = var.vpc_range
  instance_tenancy = "default"

  tags = {
    Name = "harness_test"
  }
}

// terraform {
//   backend "s3" {
//     bucket = "aj27-terraform-s3-backend"
//     key    = "tfstate_file/Calling_EKS_1/"
//     region = "us-east-1"
//   }
// }



// ############################################################################
// ###################  Route 53 With SSL Started #############################
// ############################################################################


// # module "Route53_External_DNS" {

// #   source = "git::https://github.com/Kubernetes26/EKS_Modules.git//EKS_Route_53_and_SSL_Module"

// #   Domain_Name = "ajhub.xyz"


// # }

// # output "Route53_Name_Servers" {

// #   value = module.Route53_External_DNS.Name_Server_Records
  
// # }


// ############################################################################
// ###################   Route 53 With SSL  end  ##############################
// ############################################################################



// module "EKS_VPC_AJ" {
//   source = "git::https://github.com/Kubernetes26/EKS_Modules.git//EKS_VPC" 
//   VPC_Name = "AJ_VPC" 
//   VPC_CIDR = "10.0.0.0/16" 
//   list_Public_Subnet_cidr = ["10.0.10.0/24", "10.0.20.0/24",] 
//   list_Private_Subnet_cidr = ["10.0.110.0/24", "10.0.120.0/24"] 
//   CLUSTER_NAME= "AJCLUSTER"
// }


// # module "EKS_RDS_Instance" {
// #   source = "git::https://github.com/Kubernetes26/EKS_Modules.git//EKS_RDS" 
// #   VPC_NAME = "AJ_VPC" 
// #   database_instance_identifier = "usermgmt-DB-instance1"
// #   allocated_storage = 10
// #   database_engine  = "mysql"
// #   database_engine_version = 5.7
// #   database_instance_class = "db.t2.micro"
// #   database_name = "usermgmt"
// #   database_username = "dbadmin"
// #   database_password = "dbpassword11"

// #   depends_on = [
// #     module.EKS_VPC_AJ
// #   ]  
  
// # }


// module "EKS_Cluster" {

//   source = "git::https://github.com/Kubernetes26/EKS_Modules.git//EKS_CLUSTER"

//   CLUSTER_NAME = "AJCLUSTER"
//   CLUSTER_VERSION = "1.20"
//   VPC_Name = "AJ_VPC"
//   depends_on = [
//     module.EKS_VPC_AJ
//   ]

// }

// module "EKS_Node_Group" {

//   source = "git::https://github.com/Kubernetes26/EKS_Modules.git//EKS_NODE_GROUP"

//   CLUSTER_NAME = "AJCLUSTER"
//   Node_Group_NAME = "AJ_Private_NG"
//   Node_Group_Number = 01
//   ec2_ssh_key = "key27"
//   desired_size = 2
//   max_size = 4
//   min_size = 2
//   disk_size = 10
//   instance_types = "t2.medium"
//   Private_Node_Group = false
  
//   VPC_NAME = "AJ_VPC"

//   depends_on = [
//     module.EKS_Cluster
//   ]

// }

// module "EKS_oidc_provider" {

//   source = "git::https://github.com/Kubernetes26/EKS_Modules.git//EKS_OIDC"

//   CLUSTER_NAME = "AJCLUSTER"

//   depends_on = [
//     module.EKS_Cluster
//   ]

// }

// resource "null_resource" "name1" {

//   triggers = {
//     always_run = "${timestamp()}"
//   }

//   provisioner "local-exec" {

//     command = "aws eks --region us-east-1 update-kubeconfig --name AJCLUSTER"
    
//   }
//   depends_on = [
//     module.EKS_Cluster
//   ]
  
// }



// # resource "null_resource" "name2" {

// #   triggers = {
// #     always_run = "${timestamp()}"
// #   }

// #   provisioner "local-exec" {

// #     command = "kubectl apply -k github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master"
    
// #   }
// #   depends_on = [
// #     null_resource.name1
// #   ]
  
// # }

// # resource "null_resource" "name3" {

// #   triggers = {
// #     always_run = "${timestamp()}"
// #   }

// #   provisioner "local-exec" {

// #     command = "kubectl apply -k https://github.com/kubernetes-sigs/aws-efs-csi-driver/deploy/kubernetes/overlays/stable/?ref=master"
    
// #   }

// #   depends_on = [
// #     null_resource.name2
// #   ]

// # }
